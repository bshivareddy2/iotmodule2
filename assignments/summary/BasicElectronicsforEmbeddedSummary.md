**Basic Electronics for Embedded systems**
- Embedded systems are a cornerstone of the electronics industry today. Embedded systems are used in almost all the areas like consumer, cooking, industrial, automotive, medical, commercial and military applications.
- A system is a way of working, organizing or doing one or many tasks according to a fixed plan, program or set of rules. A system is also an arrangement in which all its units assemble and work together according to the plan or program.
- As its name suggests, Embedded means something that is attached to another thing.
-  An embedded system can be thought of as a computer hardware system having software embedded in it. An embedded system can be an independent system or it can be a part of a large system.
- It offers many benefits such as sophisticated control, precision timing, low unit cost, low development cost, high flexibility, small size, and low weight. These basic characteristics can be used to improve the overall system or device in various ways:
1.Improved performance.
2.More functions and features
3.Reduced cost
4.Increased dependability 

- Embedded systems contain two main elements:

1)**Embedded system hardware**: As with any electronic system, an embedded system requires a hardware platform on which to run. The hardware will be based around a microprocessor or microcontroller. The embedded system hardware will also contain other elements including memory, input output (I/O) interfaces as well as the user interface, and the display.

2)**Embedded system software**: The embedded system software is written to perform a particular function. It is typically written in a high level format and then compiled down to provide code that can be lodged within a non-volatile memory within the hardware.

-->The various blocks of an embedded system is briefly explained below:
• Sensor – It measures the physical quantity and converts it to an electrical signal which can be read by an observer or by any electronic instrument like an A2D converter. A sensor stores the measured quantity to the memory.

• **A-D Converter** – An analog-to-digital converter converts the analog signal sent by the sensor into a digital signal.

• **Processor & ASICs** – Processors process the data to measure the output and store it to the memory.

•**D-A Converter** – A digital-to-analog converter converts the digital data fed by the processor to analog data.

• **Actuator** – An actuator compares the output given by the D-A Converter to the actual (expected) output stored in it and stores the approved output.

**Embedded System Hardware**
When using an embedded system there is a choice between the use of a microcontroller or a microprocessor.

• **Microcontroller based systems**: A microcontroller is essentially a CPU, central processor unit, or processor with integrated memory or peripheral devices. As fewer external components are needed, embedded system using microcontrollers tend to be more widely used.

•**Microprocessor based systems**: Microprocessors contain a CPU but use external chips for memory and peripheral interfaces. As they require more devices on the board, but they allow more expansion and selection of exact peripherals, etc, this approach tends to be used for the larger embedded systems.

**Embedded systems software**

One of the key elements of any embedded system is the software that is used to run the microcontroller.

• **Machine code**: Machine code is the most basic code that is used for the processor unit. The code is normally in hex code and provides the basic instructions for each operation of the processor. This form of code is rarely used for embedded systems these days.

• **Programming language**: Writing machine code is very laborious and time consuming. It is difficult to understand and debug. To overcome this, high level programming languages are often used. Languages including C, C++, etc are commonly used.

**Embedded System Architecture**

There are two basic types of embedded system architecture. When data and code lie in different memory blocks, then the architecture is referred as Harvard architecture. In case data and code lie in the same memory block, then the architecture is referred as Von Neumann architecture.

**Von Neumann Architecture**

The Von Neumann architecture was first proposed by a computer scientist John von Neumann. In this architecture, one data path or bus exists for both instruction and data. As a result, the CPU does one operation at a time. It either fetches an instruction from memory, or performs read/write operation on data. So an instruction fetch and a data operation cannot occur simultaneously, sharing a common bus.
-->Von-Neumann architecture supports simple hardware. It allows the use of a single, sequential memory. Today’s processing speeds vastly outpace memory access times, and we employ a very fast but small amount of memory (cache) local to the processor.

**Harvard Architecture**

The Harvard architecture offers separate storage and signal buses for instructions and data. This architecture has data storage entirely contained within the CPU, and there is no access to the instruction storage as data. Computers have separate memory areas for program instructions and data using internal data buses, allowing simultaneous access to both instructions and data.
- Programs needed to be loaded by an operator; the processor could not boot itself. In a Harvard architecture, there is no need to make the two memories share properties.

- Embedded systems communicate with the outside world via their peripherals, such as following:

1.Serial Communication Interfaces (SCI) like RS-232, RS-422, RS-485, etc.

2.Synchronous Serial Communication Interface like I2C, SPI, SSC, and ESSI.

3.Universal Serial Bus (USB).

4.Multi Media Cards (SD Cards, Compact Flash, etc.).

5.Networks like Ethernet, LonWorks, etc.

6.Field buses like CAN-Bus, LIN-Bus, PROFIBUS, etc.

7.Timers like PLL(s), Capture/Compare and Time Processing Units.

8.Discrete IO aka General Purpose Input/Output (GPIO).

9.Analog to Digital/Digital to Analog (ADC/DAC).

10.Debugging like JTAG, ISP, ICSP, BDM Port, BITP, and DP9 ports


---->Recent interest in hardware/software code-design is a step in the right direction, as it permits trade-offs between hardware and software that are critical for more cost-effective embedded systems. However, to be successful future tools may well need to increase scope even further to include life-cycle issues and business issues..